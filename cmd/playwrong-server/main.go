package main

import (
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/pion/rtp/codecs"
	"io"
	"log"
	"net/http"
	"sync"
	"time"

	"gitlab.com/verygoodsoftwarenotvirus/playwrong/pkg/types"

	"github.com/google/uuid"
	"github.com/pion/rtcp"
	"github.com/pion/webrtc/v3"
	"github.com/pion/webrtc/v3/pkg/media/samplebuilder"
)

const (
	rtcpPLIInterval = time.Second * 3
)

var (
	peerConnectionConfig = webrtc.Configuration{
		ICEServers: []webrtc.ICEServer{
			{
				URLs: []string{"stun:stun.l.google.com:19302"},
			},
		},
	}
)

type Stream struct {
	audioTrack, videoTrack *webrtc.TrackLocalStaticRTP
}

type webRTCBroadcastManager struct {
	localTrackHat sync.Mutex
	localTracks   map[string]*Stream
	audioBuilder  *samplebuilder.SampleBuilder
	videoBuilder  *samplebuilder.SampleBuilder
}

func NewWebRTCBroadcastManager() *webRTCBroadcastManager {
	return &webRTCBroadcastManager{
		audioBuilder: samplebuilder.New(10, &codecs.OpusPacket{}, 48000),
		videoBuilder: samplebuilder.New(10, &codecs.VP8Packet{}, 90000),
		localTracks:  map[string]*Stream{},
	}
}

func (m *webRTCBroadcastManager) BeginStreamHandler(res http.ResponseWriter, req *http.Request) {
	sdp := webrtc.SessionDescription{}
	if err := json.NewDecoder(req.Body).Decode(&sdp); err != nil {
		panic(err)
	}

	// Create a new RTCPeerConnection
	peerConnection, err := webrtc.NewPeerConnection(peerConnectionConfig)
	if err != nil {
		panic(err)
	}

	// Allow us to receive 1 video track
	if _, err = peerConnection.AddTransceiverFromKind(webrtc.RTPCodecTypeVideo); err != nil {
		panic(err)
	}

	// Allow us to receive 1 audio track
	if _, err = peerConnection.AddTransceiverFromKind(webrtc.RTPCodecTypeAudio); err != nil {
		panic(err)
	}

	streamID := uuid.New().String()

	// Set a handler for when a new remote track starts, this just distributes all our packets
	// to connected peers
	peerConnection.OnTrack(m.buildIncomingStreamTrackHandler(streamID, peerConnection))

	// Set the remote SessionDescription
	if err = peerConnection.SetRemoteDescription(sdp); err != nil {
		panic(err)
	}

	// Create answer
	answerOptions := &webrtc.AnswerOptions{OfferAnswerOptions: webrtc.OfferAnswerOptions{}}
	answer, err := peerConnection.CreateAnswer(answerOptions)
	if err != nil {
		panic(err)
	}

	// Create channel that is blocked until ICE Gathering is complete
	gatherComplete := webrtc.GatheringCompletePromise(peerConnection)

	// Sets the LocalDescription, and starts our UDP listeners
	if err = peerConnection.SetLocalDescription(answer); err != nil {
		panic(err)
	}

	// Block until ICE Gathering is complete, disabling trickle ICE
	// we do this because we only can exchange one signaling message
	// in a production application you should exchange ICE Candidates via OnICECandidate
	<-gatherComplete

	out := &types.BeginStreamResponse{
		StreamID:           streamID,
		SessionDescription: *peerConnection.LocalDescription(),
	}

	if err = json.NewEncoder(res).Encode(out); err != nil {
		panic(err)
	}
}

func (m *webRTCBroadcastManager) buildIncomingStreamTrackHandler(streamID string, peerConnection *webrtc.PeerConnection) func(*webrtc.TrackRemote, *webrtc.RTPReceiver) {
	return func(remoteTrack *webrtc.TrackRemote, _ *webrtc.RTPReceiver) {
		// Send a PLI on an interval so that the publisher is pushing a keyframe every rtcpPLIInterval
		// This can be less wasteful by processing incoming RTCP events, then we would emit a NACK/PLI when a viewer requests it
		go func() {
			ticker := time.NewTicker(rtcpPLIInterval)
			for range ticker.C {
				if rtcpSendErr := peerConnection.WriteRTCP([]rtcp.Packet{&rtcp.PictureLossIndication{MediaSSRC: uint32(remoteTrack.SSRC())}}); rtcpSendErr != nil {
					fmt.Println(rtcpSendErr)
				}
			}
		}()

		internalStreamID := uuid.New().String()
		// Create a local track, all our SFU clients will be fed via this track
		localTrack, err := webrtc.NewTrackLocalStaticRTP(remoteTrack.Codec().RTPCodecCapability, internalStreamID, streamID)
		if err != nil {
			log.Fatal(err)
		}

		m.localTrackHat.Lock()
		if _, ok := m.localTracks[streamID]; !ok {
			m.localTracks[streamID] = &Stream{}
		}

		switch localTrack.Kind() {
		case webrtc.RTPCodecTypeAudio:
			m.localTracks[streamID].audioTrack = localTrack
		case webrtc.RTPCodecTypeVideo:
			m.localTracks[streamID].videoTrack = localTrack
		}

		m.localTrackHat.Unlock()
		log.Println("incoming stream established")

		rtpBuf := make([]byte, 1500)
		for {
			i, _, readErr := remoteTrack.Read(rtpBuf)
			if readErr != nil {
				panic(readErr)
			}

			// ErrClosedPipe means we don't have any subscribers, this is ok if no peers have connected yet
			if _, writeErr := localTrack.Write(rtpBuf[:i]); writeErr != nil && !errors.Is(writeErr, io.ErrClosedPipe) {
				panic(writeErr)
			}
		}
	}
}

func (m *webRTCBroadcastManager) JoinStreamHandler(res http.ResponseWriter, req *http.Request) {
	input := types.JoinStreamRequest{}
	if err := json.NewDecoder(req.Body).Decode(&input); err != nil {
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	peerConnection, err := webrtc.NewPeerConnection(peerConnectionConfig)
	if err != nil {
		panic(err)
	}

	m.localTrackHat.Lock()
	localTrack, ok := m.localTracks[input.StreamID]
	m.localTrackHat.Unlock()
	if !ok {
		res.WriteHeader(http.StatusTooEarly)
		return
	}

	videoSender, err := peerConnection.AddTrack(localTrack.videoTrack)
	if err != nil {
		panic(err)
	}

	audioSender, err := peerConnection.AddTrack(localTrack.audioTrack)
	if err != nil {
		panic(err)
	}

	// Read incoming RTCP packets
	// Before these packets are returned they are processed by interceptors. For things
	// like NACK this needs to be called.
	go func() {
		rtcpBuf := make([]byte, 1500)
		for {
			if _, _, rtcpErr := videoSender.Read(rtcpBuf); rtcpErr != nil {
				return
			}
		}
	}()
	go func() {
		rtcpBuf := make([]byte, 1500)
		for {
			if _, _, rtcpErr := audioSender.Read(rtcpBuf); rtcpErr != nil {
				return
			}
		}
	}()

	// Set the remote SessionDescription
	err = peerConnection.SetRemoteDescription(input.SessionDescription)
	if err != nil {
		panic(err)
	}

	// Create answer
	answer, err := peerConnection.CreateAnswer(nil)
	if err != nil {
		panic(err)
	}

	// Create channel that is blocked until ICE Gathering is complete
	gatherComplete := webrtc.GatheringCompletePromise(peerConnection)

	// Sets the LocalDescription, and starts our UDP listeners
	err = peerConnection.SetLocalDescription(answer)
	if err != nil {
		panic(err)
	}

	// Block until ICE Gathering is complete, disabling trickle ICE
	// we do this because we only can exchange one signaling message
	// in a production application you should exchange ICE Candidates via OnICECandidate
	<-gatherComplete

	// Get the LocalDescription and take it to base64 so we can paste in browser
	if err = json.NewEncoder(res).Encode(*peerConnection.LocalDescription()); err != nil {
		panic(err)
	}

	log.Println("audience member established")
}

//go:embed templates/index.html
var index []byte

func main() {
	m := &webrtc.MediaEngine{}
	if err := m.RegisterDefaultCodecs(); err != nil {
		panic(err)
	}

	manager := NewWebRTCBroadcastManager()
	mux := http.NewServeMux()

	mux.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		if _, err := res.Write(index); err != nil {
			panic(err)
		}
	})

	mux.HandleFunc("/start", manager.BeginStreamHandler)
	mux.HandleFunc("/join", manager.JoinStreamHandler)

	if err := http.ListenAndServe(":3000", mux); err != nil {
		panic(err)
	}
}
