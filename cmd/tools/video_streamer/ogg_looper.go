package main

import (
	"bytes"
	"errors"
	"io"
	"log"

	"github.com/pion/webrtc/v3/pkg/media/oggreader"
)

type oggPage struct {
	header *oggreader.OggPageHeader
	data   []byte
}

// LoopingOGGReader is used to read Ogg files and return page payloads
type LoopingOGGReader struct {
	track        []oggPage
	currentIndex int
}

// NewLoopingOGGReader returns a new Ogg reader and Ogg header with an io.ReadSeeker input
func NewLoopingOGGReader(audioFile []byte) (*LoopingOGGReader, error) {
	reader := &LoopingOGGReader{
		track: collectOGGPages(audioFile),
	}

	return reader, nil
}

// ParseNextPage reads from stream and returns Ogg page payload, header,
// and an error if there is incomplete page data.
func (o *LoopingOGGReader) ParseNextPage() ([]byte, *oggreader.OggPageHeader, error) {
	if o.currentIndex == len(o.track) {
		// end of track, reset
		o.currentIndex = 0
	}

	currentPage := o.track[o.currentIndex]
	o.currentIndex += 1

	return currentPage.data, currentPage.header, nil
}

func collectOGGPages(audioFile []byte) []oggPage {
	ogg, _, err := oggreader.NewWith(bytes.NewReader(audioFile))
	if err != nil {
		log.Fatalln(err)
	}

	pages := []oggPage{}

	firstHeaderSkipped := false
	for {
		page, pageHeader, pageErr := ogg.ParseNextPage()
		if !firstHeaderSkipped { // funky pre-header thing?
			firstHeaderSkipped = true
			continue
		}

		if pageErr != nil {
			if errors.Is(pageErr, io.EOF) {
				break
			}
		}
		pages = append(pages, oggPage{
			data:   page,
			header: pageHeader,
		})
	}

	return pages
}
