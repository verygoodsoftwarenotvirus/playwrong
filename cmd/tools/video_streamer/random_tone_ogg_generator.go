package main

import (
	"bytes"
	"embed"
	"encoding/base64"
	"encoding/gob"
	"github.com/pion/webrtc/v3/pkg/media/oggreader"
	"log"
	"path/filepath"
	"strings"
)

// RandomToneOGG is used to read Ogg files and return page payloads
type RandomToneOGG struct {
	tracks       map[string][][]byte
	currentTrack [][]byte
	currentIndex int
}

// NewRandomToneOGGReader returns a new Ogg reader and Ogg header with an io.ReadSeeker input
func NewRandomToneOGGReader() (*RandomToneOGG, error) {
	tracks := getStaticTones()

	reader := &RandomToneOGG{
		tracks:       tracks,
		currentTrack: tracks["Bb4"],
	}

	return reader, nil
}

// ParseNextPage reads from stream and returns Ogg page payload, header,
// and an error if there is incomplete page data.
func (o *RandomToneOGG) ParseNextPage() ([]byte, *oggreader.OggPageHeader, error) {
	if o.currentIndex == len(o.currentTrack) {
		// end of track, reset
		o.currentIndex = 0
	}

	currentPage := o.currentTrack[o.currentIndex]
	o.currentIndex += 1

	return currentPage, nil, nil
}

//go:embed test_files/static_tones/*.ogg
var audioFiles embed.FS

func getStaticTones() map[string][][]byte {
	dirEntries, dirReadErr := audioFiles.ReadDir("test_files/static_tones")
	if dirReadErr != nil {
		log.Fatalln(dirReadErr)
	}

	outMap := map[string][][]byte{}

	for _, dirEntry := range dirEntries {
		fileName := dirEntry.Name()

		audioFile, fileReadErr := audioFiles.ReadFile(filepath.Join("test_files/static_tones", fileName))
		if fileReadErr != nil {
			log.Fatalln(fileReadErr)
		}

		data := [][]byte{}
		pages := collectOGGPages(audioFile)
		for _, page := range pages {
			data = append(data, page.data)
		}

		outMap[strings.Split(fileName, "_")[1]] = data
	}

	var b bytes.Buffer
	if encodeErr := gob.NewEncoder(&b).Encode(outMap); encodeErr != nil {
		log.Fatalln(encodeErr)
	}

	_ = base64.StdEncoding.EncodeToString(b.Bytes())

	return outMap
}
