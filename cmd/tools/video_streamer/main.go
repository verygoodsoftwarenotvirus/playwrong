package main

import (
	"bytes"
	"context"
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/pion/webrtc/v3/pkg/media"
	"github.com/pion/webrtc/v3/pkg/media/oggreader"
	"gitlab.com/verygoodsoftwarenotvirus/playwrong/pkg/types"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/pion/webrtc/v3"
	"github.com/pion/webrtc/v3/pkg/media/ivfreader"
)

var (
	// Prepare the configuration
	config = webrtc.Configuration{
		ICEServers: []webrtc.ICEServer{
			{
				URLs: []string{"stun:stun.l.google.com:19302"},
			},
		},
	}

	candidatesMux     sync.Mutex
	pendingCandidates = make([]*webrtc.ICECandidate, 0)
)

func signalCandidate(addr string, c *webrtc.ICECandidate) error {
	payload := []byte(c.ToJSON().Candidate)
	resp, err := http.Post(fmt.Sprintf("http://%s/candidate", addr), "application/json; charset=utf-8", bytes.NewReader(payload)) //nolint:noctx
	if err != nil {
		return err
	}

	if closeErr := resp.Body.Close(); closeErr != nil {
		return closeErr
	}

	return nil
}

func buildCandidateRoute(peerConnection *webrtc.PeerConnection) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		candidate, candidateErr := ioutil.ReadAll(req.Body)
		if candidateErr != nil {
			log.Panic(candidateErr)
		}

		if err := peerConnection.AddICECandidate(webrtc.ICECandidateInit{Candidate: string(candidate)}); err != nil {
			log.Panic(err)
		}
	}
}

func buildPeerConnection() (*webrtc.PeerConnection, error) {
	peerConnection, err := webrtc.NewPeerConnection(config)
	if err != nil {
		return nil, err
	}

	if _, err = peerConnection.AddTransceiverFromKind(webrtc.RTPCodecTypeAudio); err != nil {
		return nil, err
	}

	if _, err = peerConnection.AddTransceiverFromKind(webrtc.RTPCodecTypeVideo); err != nil {
		return nil, err
	}

	// When an ICE candidate is available send to the other Pion instance
	// the other Pion instance will add this candidate by calling AddICECandidate
	peerConnection.OnICECandidate(func(c *webrtc.ICECandidate) {
		if c == nil {
			return
		}

		candidatesMux.Lock()
		defer candidatesMux.Unlock()

		desc := peerConnection.RemoteDescription()
		if desc == nil {
			pendingCandidates = append(pendingCandidates, c)
		} else if onICECandidateErr := signalCandidate(answerAddr, c); onICECandidateErr != nil {
			log.Panic(onICECandidateErr)
		}
	})

	// Set the handler for Peer connection state
	// This will notify you when the peer has connected/disconnected
	peerConnection.OnConnectionStateChange(func(s webrtc.PeerConnectionState) {
		fmt.Printf("Peer Connection State has changed: %s\n", s.String())

		if s == webrtc.PeerConnectionStateFailed {
			// Wait until PeerConnection has had no network activity for 30 seconds or another failure. It may be reconnected using an ICE Restart.
			// Use webrtc.PeerConnectionStateDisconnected if you are interested in detecting faster timeout.
			// Note that the PeerConnection may come back from PeerConnectionStateDisconnected.
			fmt.Println("Peer Connection has gone to failed exiting")
			os.Exit(0)
		}
	})

	return peerConnection, nil
}

const (
	communicationInterval = time.Second
)

var (
	//go:embed test_files/binary_is_for_code_not_gender.ivf
	exampleVideoTrack []byte
	//go:embed test_files/binary_is_for_code_not_gender.ogg
	exampleAudioTrack []byte
)

func setupVideoSender(connectionCtx context.Context, peerConnection *webrtc.PeerConnection) error {
	// Create a video track
	videoTrack, videoTrackErr := webrtc.NewTrackLocalStaticSample(webrtc.RTPCodecCapability{MimeType: webrtc.MimeTypeVP8}, "video", "playwrong")
	if videoTrackErr != nil {
		return videoTrackErr
	}

	videoSender, videoTrackErr := peerConnection.AddTrack(videoTrack)
	if videoTrackErr != nil {
		return videoTrackErr
	}

	// Read incoming RTCP packets
	// Before these packets are returned they are processed by interceptors. For things
	// like NACK this needs to be called.
	go func() {
		rtcpBuf := make([]byte, 1500)
		for {
			if _, _, rtcpErr := videoSender.Read(rtcpBuf); rtcpErr != nil {
				return
			}
		}
	}()

	go func() {
		ivf, header, ivfErr := ivfreader.NewWith(bytes.NewReader(exampleVideoTrack))
		if ivfErr != nil {
			log.Panic(ivfErr)
		}

		// Wait for connection established
		<-connectionCtx.Done()

		// Send our video file frame at a time. Pace our sending, so we send it at the same speed it should be played back as.
		// This isn't required since the video is timestamped, but we will such much higher loss if we send all at once.
		sleepTime := time.Millisecond * time.Duration((float32(header.TimebaseNumerator)/float32(header.TimebaseDenominator))*1000)
		for {
			frame, _, frameErr := ivf.ParseNextFrame()
			if errors.Is(frameErr, io.EOF) {
				ivf.ResetReader(func(bytesRead int64) io.Reader {
					return bytes.NewReader(exampleVideoTrack[ivfFileHeaderSize:])
				})
			}

			if !errors.Is(frameErr, io.EOF) && frameErr != nil {
				log.Panic(frameErr)
			}

			if sampleWriteErr := videoTrack.WriteSample(media.Sample{Data: frame, Duration: time.Second}); sampleWriteErr != nil {
				log.Panic(sampleWriteErr)
			}

			time.Sleep(sleepTime)
		}
	}()

	return nil
}

func setupAudioSender(connectionCtx context.Context, peerConnection *webrtc.PeerConnection) error {
	// Create an audio track
	audioTrack, audioTrackErr := webrtc.NewTrackLocalStaticSample(webrtc.RTPCodecCapability{MimeType: webrtc.MimeTypeOpus}, "audio", "playwrong")
	if audioTrackErr != nil {
		return audioTrackErr
	}

	audioSender, audioTrackErr := peerConnection.AddTrack(audioTrack)
	if audioTrackErr != nil {
		return audioTrackErr
	}

	// Read incoming RTCP packets
	// Before these packets are returned they are processed by interceptors. For things
	// like NACK this needs to be called.
	go func() {
		rtcpBuf := make([]byte, 1500)
		for {
			if _, _, rtcpErr := audioSender.Read(rtcpBuf); rtcpErr != nil {
				return
			}
		}
	}()

	go func() {
		// Open on oggfile in non-checksum mode.
		ogg, _, oggErr := oggreader.NewWith(bytes.NewReader(exampleAudioTrack))
		if oggErr != nil {
			log.Panic(oggErr)
		}

		// Wait for connection established
		<-connectionCtx.Done()

		// Keep track of last granule, the difference is the amount of samples in the buffer
		var lastGranule uint64
		for {
			pageData, pageHeader, pageErr := ogg.ParseNextPage()
			if errors.Is(pageErr, io.EOF) {
				ogg, _, oggErr = oggreader.NewWith(bytes.NewReader(exampleAudioTrack))
				if oggErr != nil {
					log.Panic(oggErr)
				}
				time.Sleep(10 * time.Millisecond)
				continue
			} else if pageErr != nil {
				log.Panic(pageErr)
			}

			if pageHeader != nil {
				// The amount of samples is the difference between the last and current timestamp
				sampleCount := float64(pageHeader.GranulePosition - lastGranule)
				lastGranule = pageHeader.GranulePosition
				sampleDuration := time.Duration((sampleCount/48000)*1000) * time.Millisecond

				if sampleWriteErr := audioTrack.WriteSample(media.Sample{Data: pageData, Duration: sampleDuration}); sampleWriteErr != nil {
					log.Panic(sampleWriteErr)
				}

				time.Sleep(sampleDuration)
			} else {
				if sampleWriteErr := audioTrack.WriteSample(media.Sample{Data: pageData}); sampleWriteErr != nil {
					log.Panic(sampleWriteErr)
				}
			}
		}
	}()

	return nil
}

func connectToServer(peerConnection *webrtc.PeerConnection) error {
	// Create a datachannel with label 'data'
	dataChannel, err := peerConnection.CreateDataChannel("data", nil)
	if err != nil {
		return err
	}

	// Register channel opening handling
	dataChannel.OnOpen(func() {
		fmt.Printf("Data channel '%s'-'%d' open. Random messages will now be sent to any connected DataChannels every %s\n", dataChannel.Label(), dataChannel.ID(), communicationInterval.String())

		for range time.NewTicker(communicationInterval).C {
			// Send the message as text
			if sendTextErr := dataChannel.SendText(uuid.New().String()); sendTextErr != nil {
				log.Panic(sendTextErr)
			}
		}
	})

	// Register text message handling
	dataChannel.OnMessage(func(msg webrtc.DataChannelMessage) {
		fmt.Printf("Message from DataChannel '%s': '%s'\n", dataChannel.Label(), string(msg.Data))
	})

	iceConnectedCtx, iceConnectedCtxCancel := context.WithCancel(context.Background())

	if err = setupVideoSender(iceConnectedCtx, peerConnection); err != nil {
		log.Println(err)
	}

	if err = setupAudioSender(iceConnectedCtx, peerConnection); err != nil {
		log.Println(err)
	}

	// Set the handler for ICE connection state
	// This will notify you when the peer has connected/disconnected
	peerConnection.OnICEConnectionStateChange(func(connectionState webrtc.ICEConnectionState) {
		fmt.Printf("Connection State has changed %s \n", connectionState.String())
		if connectionState == webrtc.ICEConnectionStateConnected {
			iceConnectedCtxCancel()
		}
	})

	return nil
}

func initiateStreamWithServer(offer webrtc.SessionDescription, peerConnection *webrtc.PeerConnection) error {
	// Send our offer to the HTTP server listening in the other process
	payload, err := json.Marshal(offer)
	if err != nil {
		return err
	}

	log.Printf("making stream initiation request with payload %q", string(payload))
	uri := fmt.Sprintf("http://%s/start", answerAddr)
	req, err := http.NewRequest(http.MethodPost, uri, bytes.NewReader(payload))
	if err != nil {
		return err
	}

	req.Header.Set("X-SIGNAL-ADDR", "localhost:50000")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("invalid status: %d", resp.StatusCode)
	}
	log.Println("stream initiation request completed")

	beginStreamResponse := types.BeginStreamResponse{}
	if sdpErr := json.NewDecoder(resp.Body).Decode(&beginStreamResponse); sdpErr != nil {
		return sdpErr
	}

	if closeErr := resp.Body.Close(); closeErr != nil {
		return closeErr
	}

	if sdpErr := peerConnection.SetRemoteDescription(beginStreamResponse.SessionDescription); sdpErr != nil {
		return sdpErr
	}

	candidatesMux.Lock()
	defer candidatesMux.Unlock()

	log.Printf("stream ID: %q", beginStreamResponse.StreamID)

	for _, c := range pendingCandidates {
		if signalErr := signalCandidate(answerAddr, c); signalErr != nil {
			return signalErr
		}
	}

	return nil
}

const (
	offerAddr  = ":50000"
	answerAddr = "127.0.0.1:3000"

	ivfFileHeaderSize      = 32
	oggPageHeaderLen       = 27
	oggIDPagePayloadLength = 19
)

func main() {
	// Everything below is the Pion WebRTC API! Thanks for using it ❤️.

	// Create a new RTCPeerConnection
	peerConnection, err := buildPeerConnection()
	if err != nil {
		log.Panic(err)
	}

	defer func() {
		if cErr := peerConnection.Close(); cErr != nil {
			fmt.Printf("cannot close peerConnection: %v\n", cErr)
		}
	}()

	// Create a datachannel with label 'data'
	if err = connectToServer(peerConnection); err != nil {
		log.Panic(err)
	}

	// Create an offer to send to the other process
	offer, err := peerConnection.CreateOffer(&webrtc.OfferOptions{})
	if err != nil {
		log.Panic(err)
	}

	// Sets the LocalDescription, and starts our UDP listeners
	// Note: this will start the gathering of ICE candidates
	if err = peerConnection.SetLocalDescription(offer); err != nil {
		log.Panic(err)
	}

	go func() {
		<-time.After(5 * time.Second)
		if err = initiateStreamWithServer(offer, peerConnection); err != nil {
			log.Panic(err)
		}
	}()

	// A HTTP handler that allows the other Pion instance to send us ICE candidates
	// This allows us to add ICE candidates faster, we don't have to wait for STUN or TURN
	// candidates which may be slower
	http.HandleFunc("/candidate", buildCandidateRoute(peerConnection))

	if err = http.ListenAndServe(offerAddr, nil); err != nil {
		log.Panic(err)
	}
}
