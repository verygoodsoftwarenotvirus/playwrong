package main

import (
	"bytes"
	"embed"
	"encoding/base64"
	"encoding/gob"
	"errors"
	"io"
	"log"
	"path/filepath"
	"strings"

	"github.com/pion/webrtc/v3/pkg/media/oggreader"
)

//go:embed test_files/*.ogg
var audioFiles embed.FS

func main() {
	dirEntries, dirReadErr := audioFiles.ReadDir("test_files")
	if dirReadErr != nil {
		log.Fatalln(dirReadErr)
	}

	outMap := map[string][][]byte{}

	for _, dirEntry := range dirEntries {
		fileName := dirEntry.Name()

		audioFile, fileReadErr := audioFiles.ReadFile(filepath.Join("test_files", fileName))
		if fileReadErr != nil {
			log.Fatalln(fileReadErr)
		}

		outMap[strings.Split(fileName, "_")[1]] = collectPages(audioFile)
	}

	var b bytes.Buffer
	if encodeErr := gob.NewEncoder(&b).Encode(outMap); encodeErr != nil {
		log.Fatalln(encodeErr)
	}

	out := base64.StdEncoding.EncodeToString(b.Bytes())

	println(out)
}

func collectPages(audioFile []byte) [][]byte {
	ogg, _, err := oggreader.NewWith(bytes.NewReader(audioFile))
	if err != nil {
		log.Fatalln(err)
	}

	pages := [][]byte{}

	for {
		page, _, err := ogg.ParseNextPage()
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
		}
		pages = append(pages, page)
	}

	return pages
}
