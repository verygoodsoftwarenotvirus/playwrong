package main

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/pion/rtcp"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/pion/webrtc/v3"
)

const (
	answerAddr = ":60000"
	offerAddr  = "localhost:50000"
)

var (
	candidatesMux     sync.Mutex
	pendingCandidates = make([]*webrtc.ICECandidate, 0)

	//go:embed templates/index.html
	indexPage []byte

	config = webrtc.Configuration{
		ICEServers: []webrtc.ICEServer{
			{
				URLs: []string{"stun:stun.l.google.com:19302"},
			},
		},
	}
)

func signalCandidate(addr string, c *webrtc.ICECandidate) error {
	payload := []byte(c.ToJSON().Candidate)

	log.Println("making candidate request")
	resp, err := http.Post(
		fmt.Sprintf("http://%s/candidate", addr), // nolint:noctx
		"application/json; charset=utf-8",
		bytes.NewReader(payload),
	)
	if err != nil {
		return err
	}

	if closeErr := resp.Body.Close(); closeErr != nil {
		return closeErr
	}
	log.Println("candidate request succeeded")

	return nil
}

const (
	communicationInterval = 5 * time.Second
	rtcpPLIInterval       = 3 * time.Second
)

func buildPeerConnection() (*webrtc.PeerConnection, error) {
	// Create a new RTCPeerConnection
	peerConnection, err := webrtc.NewPeerConnection(config)
	if err != nil {
		return nil, err
	}

	// When an ICE candidate is available send to the other Pion instance,
	// the other Pion instance will add this candidate by calling AddICECandidate.
	peerConnection.OnICECandidate(func(c *webrtc.ICECandidate) {
		log.Println("ICE candidate received")
		if c == nil {
			return
		}

		candidatesMux.Lock()
		defer candidatesMux.Unlock()

		desc := peerConnection.RemoteDescription()
		if desc == nil {
			pendingCandidates = append(pendingCandidates, c)
		} else if onICECandidateErr := signalCandidate(offerAddr, c); onICECandidateErr != nil {
			panic(onICECandidateErr)
		}
	})

	// Set the handler for Peer connection state
	// This will notify you when the peer has connected/disconnected
	peerConnection.OnConnectionStateChange(func(s webrtc.PeerConnectionState) {
		fmt.Printf("Peer Connection State has changed: %s\n", s.String())

		if s == webrtc.PeerConnectionStateFailed {
			// Wait until PeerConnection has had no network activity for 30 seconds or another failure. It may be reconnected using an ICE Restart.
			// Use webrtc.PeerConnectionStateDisconnected if you are interested in detecting faster timeout.
			// Note that the PeerConnection may come back from PeerConnectionStateDisconnected.
			peerConnection.Close()
		}
	})

	// Register data channel creation handling
	peerConnection.OnDataChannel(func(d *webrtc.DataChannel) {
		fmt.Printf("New DataChannel %s %d\n", d.Label(), d.ID())

		// Register channel opening handling
		d.OnOpen(func() {
			fmt.Printf("Data channel '%s'-'%d' open. Random messages will now be sent to any connected DataChannels every %s\n", d.Label(), d.ID(), communicationInterval.String())

			for range time.NewTicker(communicationInterval).C {
				message := uuid.New().String()
				fmt.Printf("Sending '%s'\n", message)

				// Send the message as text
				if sendTextErr := d.SendText(message); sendTextErr != nil {
					panic(sendTextErr)
				}
			}
		})

		// Register text message handling
		d.OnMessage(func(msg webrtc.DataChannelMessage) {
			fmt.Printf("Message from DataChannel '%s': '%s'\n", d.Label(), string(msg.Data))
		})
	})

	// Register data channel creation handling
	peerConnection.OnTrack(func(remoteTrack *webrtc.TrackRemote, receiver *webrtc.RTPReceiver) {
		// Send a PLI on an interval so that the publisher is pushing a keyframe every rtcpPLIInterval
		// This can be less wasteful by processing incoming RTCP events, then we would emit a NACK/PLI when a viewer requests it
		go func() {
			ticker := time.NewTicker(rtcpPLIInterval)
			for range ticker.C {
				if rtcpSendErr := peerConnection.WriteRTCP([]rtcp.Packet{&rtcp.PictureLossIndication{MediaSSRC: uint32(remoteTrack.SSRC())}}); rtcpSendErr != nil {
					fmt.Println(rtcpSendErr)
				}
			}
		}()

		// Create a local track, all our SFU clients will be fed via this track
		localTrack, newTrackErr := webrtc.NewTrackLocalStaticRTP(remoteTrack.Codec().RTPCodecCapability, "video", "playwrong")
		if newTrackErr != nil {
			panic(newTrackErr)
		}

		rtpBuf := make([]byte, 1400)
		for {
			i, _, readErr := remoteTrack.Read(rtpBuf)
			if readErr != nil {
				panic(readErr)
			}

			// ErrClosedPipe means we don't have any subscribers, this is ok if no peers have connected yet
			written, err := localTrack.Write(rtpBuf[:i])
			if err != nil && !errors.Is(err, io.ErrClosedPipe) {
				panic(err)
			}

			log.Printf("wrote %dB of video data out", written)
		}
	})

	return peerConnection, nil
}

func buildCandidateHandler(peerConnection *webrtc.PeerConnection) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		candidate, candidateErr := ioutil.ReadAll(req.Body)
		if candidateErr != nil {
			panic(candidateErr)
		}

		if addCandidateErr := peerConnection.AddICECandidate(webrtc.ICECandidateInit{Candidate: string(candidate)}); addCandidateErr != nil {
			panic(addCandidateErr)
		}
	}
}

func buildBeginStreamHandler(peerConnection *webrtc.PeerConnection) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		sdp := webrtc.SessionDescription{}
		if err := json.NewDecoder(req.Body).Decode(&sdp); err != nil {
			panic(err)
		}

		// Allow us to receive 1 video track
		if _, err := peerConnection.AddTransceiverFromKind(webrtc.RTPCodecTypeVideo); err != nil {
			panic(err)
		}

		// Allow us to receive 1 audio track
		if _, err := peerConnection.AddTransceiverFromKind(webrtc.RTPCodecTypeAudio); err != nil {
			panic(err)
		}

		if err := peerConnection.SetRemoteDescription(sdp); err != nil {
			panic(err)
		}

		// Create an answer to send to the other process
		answerOptions := &webrtc.AnswerOptions{OfferAnswerOptions: webrtc.OfferAnswerOptions{}}
		answer, err := peerConnection.CreateAnswer(answerOptions)
		if err != nil {
			panic(err)
		}

		// Send our answer to the HTTP server listening in the other process
		payload, err := json.Marshal(answer)
		if err != nil {
			panic(err)
		}

		// Sets the LocalDescription, and starts our UDP listeners
		if err = peerConnection.SetLocalDescription(answer); err != nil {
			panic(err)
		}

		go func() {
			candidatesMux.Lock()
			for _, c := range pendingCandidates {
				if onICECandidateErr := signalCandidate(offerAddr, c); onICECandidateErr != nil {
					panic(onICECandidateErr)
				}
			}
			candidatesMux.Unlock()
		}()

		if _, responseWriteErr := res.Write(payload); responseWriteErr != nil {
			panic(responseWriteErr)
		}
	}
}

type multiConferencingServer struct {
}

func main() {
	// Create a new RTCPeerConnection
	peerConnection, err := buildPeerConnection()
	if err != nil {
		panic(err)
	}

	defer func() {
		if err = peerConnection.Close(); err != nil {
			fmt.Printf("cannot close peerConnection: %v\n", err)
		}
	}()

	// A HTTP handler that allows the other Pion instance to send us ICE candidates
	// This allows us to add ICE candidates faster, we don't have to wait for STUN or TURN
	// candidates which may be slower
	http.HandleFunc("/candidate", buildCandidateHandler(peerConnection))

	// A HTTP handler that processes a SessionDescription given to us from the other Pion process
	http.HandleFunc("/start", buildBeginStreamHandler(peerConnection))

	// A HTTP handler that processes a SessionDescription given to us from the other Pion process
	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		res.Write(indexPage)
	})

	// Start HTTP server that accepts requests from the offer process to exchange SDP and Candidates
	if err = http.ListenAndServe(answerAddr, nil); err != nil {
		panic(err)
	}
}
