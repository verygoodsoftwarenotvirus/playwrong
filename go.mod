module gitlab.com/verygoodsoftwarenotvirus/playwrong

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2 // indirect
	github.com/google/uuid v1.2.0
	github.com/hajimehoshi/ebiten v1.12.12
	github.com/pion/interceptor v0.0.13
	github.com/pion/logging v0.2.2
	github.com/pion/randutil v0.1.0
	github.com/pion/rtcp v1.2.6
	github.com/pion/rtp v1.6.5
	github.com/pion/transport v0.12.3
	github.com/pion/webrtc/v2 v2.2.26 // indirect
	github.com/pion/webrtc/v3 v3.0.30
	gopkg.in/metakeule/loop.v4 v4.0.0-20150407120014-b2e5c94f7247 // indirect
)
