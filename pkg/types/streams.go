package types

import (
	"github.com/pion/webrtc/v3"
)

type BeginStreamResponse struct {
	StreamID           string                    `json:"streamID"`
	SessionDescription webrtc.SessionDescription `json:"sessionDescription"`
}

type JoinStreamRequest struct {
	StreamID           string                    `json:"streamID"`
	SessionDescription webrtc.SessionDescription `json:"sessionDescription"`
}
